object PlainPermutations {

  def permuteImpl(text: Seq[Char]): List[String] = { // a string is naturally cast as a Seq[Char]
    text match { // a Seq is suitable for pattern matching
      case Nil => List()
      case Seq(head) => List(head.toString)
      case seq =>
        seq.flatMap(el => permuteImpl(seq.diff(Seq(el))).map(p => el +: p)).toList
        // diff is the difference between two Seqs
        // The function used as an argument to flatmap turns each char into a List[Seq[Char]].
        // Because the map is flat, in any such lists it gets rid of the List wrapper.
        // flatmap here acts as an aggregator of the recursive function
    }
  }

  def apply(text: String): List[String] = {
    permuteImpl(text)
  }

}
