case class Word(content: String = "") {

  def this(aChar: Char) = {
    this(aChar.toString)
  }

  override def toString: String = this.content

  def pullOut(anInt : Int): (Char, Word) = (this.content.charAt(anInt), Word(this.content.take(anInt) + this.content.drop(anInt + 1)))

  def prepend(aChar: Char): Word = {
    Word(aChar + this.content)
  }

  def permute(n: Int = 0): List[Word] = {

    if (n >= this.content.length) {
      return List()
    }

    if (this.content.length <= 1) {
      return List(this)
    }

    val (pivot, rest) = this.pullOut(n)
    rest.permute().map(_.prepend(pivot)) ++ permute(n + 1)
  }
}
