object WordPermutations {
  def main(args: Array[String]) : Unit = {
    Word(if (args.isEmpty) "atro" else args(0)).permute().foreach(println)
  }
}
