import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite

class WordSuite extends AnyFunSuite with GivenWhenThen {

  test("A Word can partially disclose its content") {
    Given("a Word")
    val word = Word("ciao")

    Then("turns into a string by exposing its content")
    assert(word.toString == "ciao")

    Then("can be split into head and tail")
    assert(Word("ciao").pullOut(0) == ('c', Word("iao")))

    Then("can return a suffix, split into head and tail")
    assert(Word("ciao").pullOut(1) == ('i', Word("cao")))
  }

  test("A character can be prepended to a Word") {
    Given("a character")
    val character = 'c'
    And("a Word")
    val word = Word("iao")
    Then("the character can be prepended to the Word")
    assert(word.prepend(character) == Word("ciao"))
  }

}
