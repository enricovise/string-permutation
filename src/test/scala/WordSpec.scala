import org.scalatest.funspec.AnyFunSpec

class WordSpec extends AnyFunSpec {

  describe("An empty word") {
    it("should have no permutations") {
      assert(Word().permute() == List())
    }
  }

  describe("A 1-letter word") {
    it("should have one permutation") {
      assert(Word("f").permute() == List(Word("f")))
    }
  }

  describe("A 2-letter word") {
    it("should have two permutations") {
      assert(Word("al").permute() == List(Word("al"), Word("la")))
    }
  }

  describe("A 3-letter word") {
    it("should have six permutations") {
      assert(Word("obi").permute() == List(Word("obi"), Word("oib"), Word("boi"), Word("bio"), Word("iob"), Word("ibo")))
    }
  }
}
